import java.util.Map;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
    


/// Interface ObjVlisp
interface ObjVlisp extends OObjet{
    OObjet getClasse(String nomDeClasse);
}


/// Classe ObjVlispFabrique
class ObjVlispFabrique implements ObjVlisp{

    List <Classe> listeClasse = new ArrayList<Classe>();

    public ObjVlispFabrique() { // Initialise la classe System et Class ? 
        listeClasse.add(new Classe("System"));
        listeClasse.add(new Classe("Class"));
    }

    static ObjVlisp nouveau() {
        return new ObjVlispFabrique();
    }

    public Object message(String nom, Object... arguments) {
        return null;
    }

    
    public Object superMessage(String nom, Object... argements) {
        return null;
    }

    public Object erreur(String cause) {
        return null;
    }
    
    public OObjet getClasse(String nomDeClasse) {
        // Parcourir la liste de classe, si dedans, fais .nomClasse, sinon retourne null 
        for (int i = 0; i< listeClasse.size();i++) {
            String name = listeClasse.get(i).nomClasse;
            if (name == nomDeClasse) return (OObjet) (listeClasse.get(i));
        }
        return null;
    }

}
    
/// Interface Oobjet
interface OObjet {
    Object message(String nom, Object ... arguments);
    Object superMessage(String nom,Object ... argements);
    Object erreur(String cause);


}

/// Classe Objet 
class Objet extends Classe {

    public Objet(java.lang.String nom) {
        super(nom);
    }

    Classe classe;
    Map<String,?> messages;

    String String(){
        String res="";
        for (int i = 0; i < this.nomAttributs.size();i++) {
            res+=this.nomAttributs.get(i);
            }
        return res;
        }
   
        /* Classe nouveau() {
            message(:nouveau, Map.of("nomClasse","A"))
        } */
    }
    
    /// Classe classe
    class Classe implements OObjet {
        String nomClasse;
        List <String> nomAttributs = new ArrayList<>();
        Map <String,Object> messages = new HashMap<>();
        Objet superClasse;

        public Map<String, ?> getMessages() {
            return messages;
        }

        public Classe(String nom) {
            this.nomClasse = nom;
        }

        public Object message(String nom, Object ... arguments){
            
            Object[] test = arguments;
            if (nom == ":nouveau") { // Censé créer une nouvelle classe mais la modifie ici
                if (arguments != null) {
                    for (Object Entry : test ) { // On est censé appliquer les arguments 
                        Class<? extends Object> name = Entry.getClass();
                        String nameType = name.getSimpleName();
                        String newName = nameType.substring(0,nameType.length()-1);
                        if (newName.equals("Map")) { // Si l'argument est une map 
                            // Doit récuperer les valeurs donnée dans le map of
                            for (Entry<String, ?> entry : ((Map<String, ?>) Entry).entrySet()) {
                                String key = entry.getKey();
                                Object value = entry.getValue();
                                this.messages.put(key,value);
                                this.nomAttributs.add(key);
                            }
                        }
                        if (newName.equals("List")) { // Si list.of
                            List<Object> oui = (List<Object>) Entry;
                            System.out.println(oui);
                            for (Object val : oui) {
                                System.out.println(val);
                                this.nomAttributs.add((String) val);
                            }
                        }
                    }
                }
                else { // Pas d'arguments, crée un nouvel objet
                   return (OObjet) new Objet((this.nomClasse)); // Cree un nouvel objet
                }
            }
                if (nom == "nouveau") { // Nouveau utilisé seul
                    return (OObjet) new Objet(this.nomClasse); // Cree un nouvel objet
                }
                if (nom.equals("toString") || nom.equals(":toString")) {
                    System.out.println("Nom : "+this.nomClasse);
                    System.out.println("Attributs : "+this.nomAttributs);
                    System.out.println("Messages "+ this.messages);
                }

                if (nom == "message" || nom == ":message") { // Ajoute des messages a la classe courante
                    
                }
            return this;
        }
        
        @Override
        public Object superMessage(String nom, Object... argements) {
            Object obj = chercherClasse(this,nom);
            if(obj != null){
                return 1;
            }
            return erreur("erreur dans le message");
        }


        private Objet chercherClasse(Classe classe,String nom){
            /*
            var actuel = obj.classe();
            while(actuel != null){
                Map<String,Message> msg = messages(actuel);
                if(messages.get(nom) != null){
                    return actuel;
                }
                actuel = actuel.getMessages().get("superClass"));
            }
            */
            return null;
        }

        
        @Override
        public Object erreur(String cause) {
            return null;
        }
    }

/// Interface Message
interface Message {
        Object apply(OObjet unObjet, Object ... arguments);
}
    



